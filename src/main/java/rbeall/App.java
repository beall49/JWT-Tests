package rbeall;

/**
 * Hello world!
 */
public class App {

  public static void main(String[] args) {
    JWTBuilder jwt = new JWTBuilder();
    String newToken = jwt.getNewToken("user", "password");
    System.out.println(newToken);
  }
}
