package rbeall;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.HashMap;

public class JWTBuilder {
  HashMap<String, Object> headers;
  String user, pass;
  Algorithm algo;
  long iat = 0;
  final long EXPIRATION = 1209600L;
  static final String UNAUTHORIZED = "Unauthorized";
  final private String
          ISSUER = "https://www.example.com/",
          SECRET = "MYSECRET",
          AUDIENCE = "portal",
          V_USER = "user",
          V_PASS = "password",
          NAME = "name",
          PASS = "pass",
          EXP = "exp",
          IAT = "iat",
          AUD = "aud";

  JWTBuilder() {
    this.algo = getAlgorithm();
  }

  JWTBuilder(String _secret) {
    this.algo = getAlgorithm(_secret);
  }

  public String getNewToken(String _user, String _pass) {
    this.user = _user;
    this.pass = _pass;

    if (isInvalidUser(user, pass)) {
      return UNAUTHORIZED;
    }

    /*
    * puts our own stuff inside the token
    */
    headers = getHeaders();

    return com.auth0.jwt.JWT.create()
            .withIssuer(ISSUER)
            .withHeader(headers)
            .sign(algo);
  }

  public String verifyToken(String token) {
    try {
      DecodedJWT jwt = getVerifier().verify(token);
      String token_user = jwt.getHeaderClaim(NAME).asString();
      String token_pass = jwt.getHeaderClaim(PASS).asString();
      long token_exp = jwt.getHeaderClaim(EXP).asInt();

      /*
      * is this user unauthorized
      */
      if (isInvalidUser(token_user, token_pass)) {
        return UNAUTHORIZED;
      }

      /*
      * if expired it returns invalid which will force the user to log
      * back in
      */
      if (token_exp < getCurrentTime()) {
        return UNAUTHORIZED;
      }

      /*
      * if all passed then refresh token
      */
      return this.getNewToken(token_user, token_pass);

    } catch (JWTVerificationException e) {
      e.printStackTrace();
      return UNAUTHORIZED;
    }
  }

  /*
  * creates the header info for the token
  */
  public HashMap<String, Object> getHeaders() {
    HashMap<String, Object> headers = new HashMap<String, Object>();
    iat = getCurrentTime();
    headers.put(AUD, AUDIENCE);
    headers.put(NAME, user);
    headers.put(PASS, pass);
    headers.put(EXP, iat + EXPIRATION);
    headers.put(IAT, iat);
    return headers;
  }

  /*
  * returns token verifier object
  */ 
  private JWTVerifier getVerifier() {
    return com.auth0.jwt.JWT.require(this.algo)
            .withIssuer(this.ISSUER)
            .build();
  }

  /*
  * returns encryption algorithm
  */
  public Algorithm getAlgorithm() {
    try {
      return Algorithm.HMAC256(SECRET);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public Algorithm getAlgorithm(String _secret) {
    try {
      return Algorithm.HMAC256(_secret);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public long getCurrentTime() {
    return System.currentTimeMillis() / 1000L;
  }

  /*
  * checks if NOT authorized
  */
  public boolean isInvalidUser(String user, String pass) {
    /* if either doesn't match */
    return (!user.equals(V_USER) || !pass.equals(V_PASS));
  }
}
