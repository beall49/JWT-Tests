package rbeall;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static rbeall.JWTBuilder.UNAUTHORIZED;

/**
 * Unit test for simple App.
 */
public class AppTest {
  JWTBuilder JWTBuilder, JWTBuilderSecret;
  static final String TOKEN = "eyJuYW1lIjoidXNlciIsImF1ZCI6InBvcnRhbCIsImV4cCI6MTQ5NDEzNTE0OCwicGFzcyI6InBhc3N3b3JkIiwiaWF0IjoxNDkyOTI1NTQ4LCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5leGFtcGxlLmNvbS8ifQ.jGxd8R5ercCXrnEfcDowULluCjpDBBHyl1UYU7xmyac";
  
  @Before public void setUp() throws Exception {
    JWTBuilder = new JWTBuilder();
    JWTBuilderSecret = new JWTBuilder("ryan-beall-secret");
  }

  @Test public void passGoodCreds(){
    String returnPayload = JWTBuilder.getNewToken("user", "password");
    assertNotEquals(UNAUTHORIZED, returnPayload);
  }

  @Test public void failBadCreds(){
    /*using bad username password*/
    String returnPayload = JWTBuilder.getNewToken("ryan", "beall");
    assertEquals(UNAUTHORIZED, returnPayload);
  }
 
  @Test public void validateToken(){
    String returnPayload = JWTBuilder.verifyToken(TOKEN);
    assertNotEquals(UNAUTHORIZED, returnPayload);
  }

  @Test public void failValidateToken(){
    /* Test using different seret */
    String returnPayload = JWTBuilderSecret.verifyToken(TOKEN);
    assertEquals(UNAUTHORIZED, returnPayload);
  }

  @After public void tearDown() throws Exception {
    JWTBuilder = null;
    JWTBuilderSecret = null;
  }
}
